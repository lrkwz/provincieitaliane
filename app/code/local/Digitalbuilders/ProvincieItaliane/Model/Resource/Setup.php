<?php
/* @var $installer Digitalbuilders_ProvinceItaliane_Model_Resource_Setup */
$installer = $this;

$data = $this->getProvinces();
foreach ($data as $code => $name) {
	$bind = array(
		'country_id'    => 'IT',
		'code'          => $code,
		'default_name'  => $name,
	);
	$installer->getConnection()->insert($installer->getTable('directory/country_region'), $bind);
	$regionId = $installer->getConnection()->lastInsertId($installer->getTable('directory/country_region'));


	$bind = array(
		'locale'    => 'it_IT',
		'region_id' => $regionId,
		'name'      => $name
	);
	$installer->getConnection()->insert($installer->getTable('directory/country_region_name'), $bind);
}
